package shrtn_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/etomyutikos/shrtn"
)

func TestShorten(t *testing.T) {
	tests := []struct {
		name   string
		input  string
		output string
	}{
		{
			"long url",
			"http://www.test.com/some/long/url",
			"b0p6zXLxnh0",
		},
		{
			"short url",
			"s.com",
			"s.com",
		},
		{
			"ridiculous",
			strings.Repeat("s", 101),
			"SzFM0MNjxeA",
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.output, Shorten(test.input))
		})
	}
}
