# shrtn
A simple HTTP server for shortening URLs using an FNV hash.

## endpoints

### /shorten
```
https://shrtn-188519.appspot.com/shorten?u=http%3A%2F%2Fwww.shrtn.test
```
The primary endpoint for shortening the URL.

Returns a `200 (OK)`. The hash will be in the `Location` header.

#### `u` query param [required]
A URL encoded URL to be shortened.

### /{hash}
```
https://shrtn-188519.appspot.com/ahVUrAfnye0
```
A hash (returned from `/shorten`) to be resolved into a full URL.

Returns a `301 (Moved Permanently)`. The URL will be in the `Location` header.
