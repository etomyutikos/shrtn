package shrtn_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/go-chi/chi"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	. "gitlab.com/etomyutikos/shrtn"
	"gitlab.com/etomyutikos/shrtn/internal/store"
	"gitlab.com/etomyutikos/shrtn/internal/store/storetest"
)

func TestExpandHandler(t *testing.T) {
	u := "http://www.shrtn.test"
	h := "ahVUrAfnye0"

	t.Run("success", func(t *testing.T) {
		var db storetest.MockStorer
		db.On("Retrieve", h).Return(u, nil)

		router := chi.NewRouter()
		router.Get(fmt.Sprintf("/{%s}", HashParamKey), ExpandHandler(&db))

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/"+url.QueryEscape(h), nil)
		router.ServeHTTP(w, r)

		assert.Equal(t, http.StatusMovedPermanently, w.Code)
		assert.Equal(t, u, w.Header().Get("Location"))
	})

	t.Run("not found", func(t *testing.T) {
		var db storetest.MockStorer
		db.On("Retrieve", h).Return("", store.ErrNotFound)

		router := chi.NewRouter()
		router.Get("/", ExpandHandler(&db))

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/"+url.QueryEscape(h), nil)
		router.ServeHTTP(w, r)

		assert.Equal(t, http.StatusNotFound, w.Code)
	})
}

type mockShortener struct {
	mock.Mock
}

func (m *mockShortener) Shorten(u string) string {
	args := m.Called(u)
	return args.String(0)
}

func Test_shortenHandler(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		u := "http://www.shrtn.test"
		h := "ahVUrAfnye0"
		var shrtnr mockShortener
		shrtnr.On("Shorten", u).Return(h).Once()

		var db storetest.MockStorer
		db.On("Store", h, u).Return(nil).Once()

		router := chi.NewRouter()
		router.Get("/", ShortenHandler(&shrtnr, &db))

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/?u="+url.QueryEscape(u), nil)

		router.ServeHTTP(w, r)

		assert.Equal(t, http.StatusOK, w.Code)
		assert.Equal(t, "/"+h, w.Header().Get("Location"))
	})
}
