package shrtn

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/etomyutikos/shrtn/internal/store"
)

const HashParamKey = "hash"

func ExpandHandler(db store.Storer) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h := chi.URLParam(r, HashParamKey)
		if h == "" {
			http.Error(w, "missing hash url param", http.StatusBadRequest)
			return
		}

		u, err := db.Retrieve(h)
		if err == store.ErrNotFound {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		} else if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Location", u)
		w.WriteHeader(http.StatusMovedPermanently)
	})
}

func ShortenHandler(s Interface, db store.Storer) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u := r.URL.Query().Get("u")
		if u == "" {
			http.Error(w, "missing u query param", http.StatusBadRequest)
			return
		}

		h := s.Shorten(u)
		if err := db.Store(h, u); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Location", "/"+h)
	})
}
