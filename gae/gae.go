package shrtn

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/etomyutikos/shrtn"
	"gitlab.com/etomyutikos/shrtn/internal/store"
)

var (
	shortenEndpoint = "/shorten"
	expandEndpoint  = fmt.Sprintf("/{%s}", shrtn.HashParamKey)
)

func init() {
	var shrtnr shrtn.Shortener
	var db store.Memory

	r := chi.NewRouter()
	r.Get(shortenEndpoint, shrtn.ShortenHandler(shrtnr, &db))
	r.Get(expandEndpoint, shrtn.ExpandHandler(&db))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	http.Handle("/", chi.ServerBaseContext(ctx, r))
}
