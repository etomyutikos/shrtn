package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"

	"github.com/go-chi/chi"

	"gitlab.com/etomyutikos/shrtn"
	"gitlab.com/etomyutikos/shrtn/internal/store"
)

var (
	shortenEndpoint = "/shorten"
	expandEndpoint  = fmt.Sprintf("/{%s}", shrtn.HashParamKey)
)

func main() {
	log.Printf("shrtn start")
	defer log.Printf("shrtn end")

	var shrtnr shrtn.Shortener
	var db store.Memory

	r := chi.NewRouter()
	r.Get(shortenEndpoint, shrtn.ShortenHandler(shrtnr, &db))
	r.Get(expandEndpoint, shrtn.ExpandHandler(&db))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	srv := http.Server{
		Addr:    ":3333",
		Handler: chi.ServerBaseContext(ctx, r),
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("error shutting down: %s", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, os.Kill)
	<-quit

	log.Printf("shutting down...")
	srv.Shutdown(ctx)
}
