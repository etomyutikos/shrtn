package shrtn

import "testing"

func BenchmarkShorten(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Shorten("http://www.shrtn.test")
	}
}
