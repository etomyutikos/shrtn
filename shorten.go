package shrtn

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"hash/fnv"
)

type Interface interface {
	Shorten(string) string
}

type Shortener struct{}

func (s Shortener) Shorten(url string) string {
	h := fnv.New64()
	h.Write([]byte(url))

	b := bytes.NewBuffer([]byte{})
	binary.Write(b, binary.BigEndian, h.Sum64())

	e := base64.RawURLEncoding.EncodeToString(b.Bytes())
	if len(e) >= len(url) {
		return url
	}
	return e
}

var Default Shortener

func Shorten(url string) string {
	return Default.Shorten(url)
}
