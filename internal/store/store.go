package store

import "errors"

type Storer interface {
	Store(hash string, url string) error
	Retrieve(hash string) (string, error)
}

var ErrNotFound = errors.New("could not find url for given hash")
