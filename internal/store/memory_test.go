package store_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	. "gitlab.com/etomyutikos/shrtn/internal/store"
)

func TestMemory(t *testing.T) {
	var m Memory

	h := "x0ZI07isO3c"
	u := "http://www.test.com"

	t.Run("Store", func(t *testing.T) {
		require.NoError(t, m.Store(h, u))
	})

	t.Run("Retrieve", func(t *testing.T) {
		actual, err := m.Retrieve(h)
		require.NoError(t, err)
		assert.Equal(t, u, actual)
	})
}
