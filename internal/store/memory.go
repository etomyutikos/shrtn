package store

import "sync"

type Memory struct {
	sync.RWMutex
	d map[string]string
}

func (m *Memory) Store(h string, u string) error {
	m.Lock()
	defer m.Unlock()

	if m.d == nil {
		m.d = make(map[string]string)
	}

	m.d[h] = u
	return nil
}

func (m *Memory) Retrieve(h string) (string, error) {
	if m.d == nil {
		return "", nil
	}

	m.RLock()
	defer m.RUnlock()

	u, ok := m.d[h]
	if !ok {
		return "", ErrNotFound
	}

	return u, nil
}
