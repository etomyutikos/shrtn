package storetest

import (
	"github.com/stretchr/testify/mock"

	"gitlab.com/etomyutikos/shrtn/internal/store"
)

type MockStorer struct {
	mock.Mock
}

var _ store.Storer = &MockStorer{}

func (m *MockStorer) Store(h, u string) error {
	args := m.Called(h, u)
	return args.Error(0)
}

func (m *MockStorer) Retrieve(h string) (string, error) {
	args := m.Called(h)
	return args.String(0), args.Error(1)
}
